import ProductCard, { ProductCardProps } from "@/components/productCard";
import Head from "next/head";
import Image from "next/image";
import { useCallback, useEffect, useState } from "react";

export default function DashboardPage() {
	const [product, setProduct] = useState<ProductCardProps[]>([]);

	const getProduct = useCallback(async () => {
		const response = await fetch("https://dummyjson.com/products").then((res) => res.json());
		setProduct(response.products);
	}, []);

	useEffect(() => {
		getProduct();

		return () => {
			setProduct([]);
		};
	}, [getProduct]);

	return (
		<>
			<Head>
				<title>Dashboard</title>
			</Head>

			<main>
				<nav className="w-full px-4 py-6 bg-white flex justify-between items-center shadow sm:px-16">
					<Image src={"/assets/logo.png"} width={35} height={35} alt="LOGO" unoptimized />

					<div className="nav-profile">
						<Image
							src={"/assets/avatar-1.png"}
							className="rounded-full"
							width={35}
							height={35}
							alt="Avatar"
							unoptimized
						/>
					</div>
				</nav>

				<section id="main-dashboard" className="px-4 mt-4 sm:px-16">
					<h1 className="text-lg font-semibold">Dashboard</h1>
					<span>Product List</span>

					<div className="grid sm:grid-cols-5 mt-3 gap-4 grid-cols-2">
						{product.map((p, i) => {
							return (
								<ProductCard
									key={i}
									thumbnail={p.thumbnail}
									category={p.category}
									description={p.description}
									price={p.price}
									rating={p.rating}
									title={p.title}
								/>
							);
						})}
					</div>
				</section>
			</main>
		</>
	);
}
