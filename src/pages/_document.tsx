import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="en">
      <Head />
      <body className='bg-purple-50'>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
