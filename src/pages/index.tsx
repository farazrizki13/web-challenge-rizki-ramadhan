import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import { FormEvent, useCallback, useState } from "react";

export default function SignInPage() {
	const router = useRouter();
	const [userId, setUserId] = useState("");
	const [password, setPassword] = useState("");
	const [errFetch, setErrFetch] = useState(false);

	const [error, setError] = useState({
		userId: false,
		password: false,
	});

	const setLoginUser = useCallback(async () => {
		try {
			const response = await fetch("https://dummyjson.com/auth/login", {
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify({
					username: userId,
					password: password,
				}),
			});

			if (response.status >= 400) {
				throw new Error("Invalid Credentials");
			} else {
				await response.json();
				setErrFetch(false);

				router.push("/dashboard");
			}
		} catch (error) {
			setErrFetch(true);
		}
	}, [userId, password, router]);

	const onFormSubmit = (e: FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		setError({
			userId: userId === "",
			password: password === "",
		});

		if (userId !== "" && password !== "") {
			setLoginUser();
		}
	};

	return (
		<>
			<Head>
				<title>Sign In | Dashboard</title>
			</Head>

			<main>
				<div className="w-32 h-32 absolute sm:w-52 sm:h-52 top-0 left-0">
					<Image
						src={"/assets/header-login.png"}
						fill
						alt="tes"
					/>
				</div>

				<div className="mx-4 sm:mx-auto sm:bg-white sm:rounded-3xl sm:shadow sm:px-6 sm:py-10 sm:w-[350px] sm:mt-14">
					<div className="w-full flex items-center justify-center py-16 sm:py-8">
						<Image src={"/assets/logo.png"} width={125} height={125} alt="logo" />
					</div>

					<div className="mt-8">
						<h5 className="text-2xl font-bold">Login</h5>
						<span className="text-sm">Please sign in to continue.</span>

						{errFetch && (
							<div className="w-full rounded-xl bg-red-400 px-4 py-2 my-2">
								<span className="text-white">Invalid Credentials</span>
							</div>
						)}

						<form className="mt-4" onSubmit={onFormSubmit}>
							<div className="mb-3 block">
								<label htmlFor="userid">User ID</label>
								<input
									type="text"
									placeholder="Enter user id"
									className="w-full px-4 py-2 mt-2"
									value={userId}
									onChange={(e) => setUserId(e.target.value)}
								/>
								<small className="text-red-500">
									{error.userId ? "User ID Is required" : ""}
								</small>
							</div>
							<div className="mb-3 block">
								<label htmlFor="password">Password</label>
								<input
									type="password"
									placeholder="Enter password"
									className="w-full px-4 py-2 mt-2"
									value={password}
									onChange={(e) => setPassword(e.target.value)}
								/>
								<small className="text-red-500">
									{error.password ? "Password Is required" : ""}
								</small>
							</div>
							<div className="w-full flex justify-end mt-3">
								<button
									className="bg-purple-600 px-8 py-3 text-white rounded-full text-sm font-semibold"
									type="submit">
									SIGN IN
								</button>
							</div>
						</form>

						<div className="w-full text-center mt-20">
							<span className="text-sm">
								Dont have an account? <strong className="text-red-500">Sign Up</strong>
							</span>
						</div>
					</div>
				</div>
			</main>
		</>
	);
}
