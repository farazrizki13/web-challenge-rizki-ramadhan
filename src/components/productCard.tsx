import Image from "next/image";
import React from "react";

export interface ProductCardProps {
	thumbnail: string;
	category: string;
	title: string;
	price: number;
	rating: string;
	description: string;
}

export default function ProductCard(props: ProductCardProps) {
	return (
		<div className="bg-white rounded-2xl">
			<div className="w-full h-36 sm:h-56 relative rounded-t-2xl">
				<Image src={props.thumbnail} alt="product" fill className="rounded-2xl" unoptimized loading="lazy" />
			</div>
			<div className="py-4 px-2">
				<div className="categories w-full flex mb-2">
					<div className="border-red-400 border-2 rounded-full text-xs text-red-400 px-2 py-1">
						{props.category}
					</div>
				</div>
				<h3 className="text-sm font-semibold">{props.title}</h3>
				<span className="text-purple-500 text-sm">${props.price}</span>
				<br />
				<span className="text-sm">
					Rating : <strong>{props.rating}</strong>
				</span>
				<hr className="mb-3" />
				<span className="text-xs">{props.description}</span>
			</div>
		</div>
	);
}
